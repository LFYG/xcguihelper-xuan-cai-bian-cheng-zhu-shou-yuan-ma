// stdafx.cpp : source file that includes just the standard includes
//	DemoManager.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "pugixml.cpp"
// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

void MyGetDirectory(wchar_t* pDir, int size)
{
	if(0!=GetModuleFileNameW(NULL,pDir,size))
	{
		wchar_t *pFilePath=wcsrchr(pDir, L'\\' );
		if(pFilePath)
		{
			pFilePath[0]=L'\0';
		}
	}
}


unsigned long XC_GetFileSize(wchar_t* pFile,unsigned long * pFileSizeHight)
{
	HANDLE hFile = CreateFileW(pFile,
		GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	unsigned long nSize = GetFileSize(hFile,pFileSizeHight);
	CloseHandle(hFile);
	return nSize;
}

VOID XC_OpenFloder(wchar_t* pDir)
{
	wchar_t lpCmd[MAX_PATH*4] = {0};
	wsprintfW(lpCmd,L"%s\"%s\"",L"explorer.exe ",pDir);
	STARTUPINFOW si={0};
	PROCESS_INFORMATION pi;
	si.cb = sizeof(si);
	CreateProcessW(NULL,lpCmd,NULL,NULL,NULL,0,NULL,NULL,&si,&pi);
	CloseHandle(pi.hProcess);
}
int XC_CopyFile(wchar_t* pSrc,wchar_t* pDes)
{
	SHFILEOPSTRUCTW fileOp ={0};
	fileOp.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR ;
	fileOp.pFrom = pSrc;
	fileOp.pTo   = pDes;
	fileOp.wFunc = FO_COPY;
	return SHFileOperationW(&fileOp);
}
BOOL XC_IsDirectory(wchar_t* lpFile)
{
	if (GetFileAttributesW(lpFile) == FILE_ATTRIBUTE_DIRECTORY)
		return TRUE;
	return FALSE;
}

BOOL XC_DeleteFolder(const wchar_t* lpszPath) 
{ 
	SHFILEOPSTRUCTW FileOp; 
	ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCTW));
	
	FileOp.fFlags = FOF_NOCONFIRMATION; 
	FileOp.hNameMappings = NULL; 
	FileOp.hwnd = NULL; 
	FileOp.lpszProgressTitle = NULL; 
	FileOp.pFrom = lpszPath; 
	FileOp.pTo = NULL; 
	FileOp.wFunc = FO_DELETE; 
	
	return SHFileOperationW(&FileOp) == 0;
}

/////////////////////////////////////
//ReNameFolder
//参数：lpszFromPath 源文件夹路径 。lpszToPath 目的文件夹路径
//作用：修改原文件夹的名字。
//
/////////////////////////////////////
BOOL XC_ReNameFolder(const wchar_t* lpszFromPath,const wchar_t* lpszToPath)
{
// 	int nLengthFrm = strlen(lpszFromPath);
// 	char *NewPathFrm = new char[nLengthFrm+2];
// 	strcpy(NewPathFrm,lpszFromPath);
// 	NewPathFrm[nLengthFrm] = L'\0';
// 	NewPathFrm[nLengthFrm+1] = L'\0';
	
    SHFILEOPSTRUCTW FileOp;
    ZeroMemory((void*)&FileOp,sizeof(SHFILEOPSTRUCTW));
    FileOp.fFlags = FOF_CONFIRMMOUSE;//FOF_NOCONFIRMATION ;
    FileOp.hNameMappings = NULL;
    FileOp.hwnd = NULL;
    FileOp.lpszProgressTitle = NULL;
    FileOp.pFrom = lpszFromPath;
    FileOp.pTo = lpszToPath;
    FileOp.wFunc = FO_RENAME;
	
    return SHFileOperationW(&FileOp) == 0;
}
bool XC_IsFileExsit(const wchar_t* file)
{
	FILE* hFile = _wfopen(file,L"r");
	if (hFile != NULL)
	{
		fclose(hFile);
		return true;
	}
	return false;
}
HXCGUI XC_GetObjectByRes(HWINDOW hWindow,const wchar_t* lpResIDName)
{
	int nId = XC_GetResIDValue(lpResIDName);
	return XC_GetObjectByID(hWindow,nId);
}
HELE XC_GetHEleByRes(HWINDOW hWindow,const wchar_t* lpResIDName)
{
	return (HELE)XC_GetObjectByRes(hWindow,lpResIDName);
}

HXCGUI XC_GetShapeByRes(HWINDOW hWindow,const wchar_t* lpResIDName)
{
	return XC_GetObjectByRes(hWindow,lpResIDName);
}